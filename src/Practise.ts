// import React from "react";

// TypeScript Supports if it is a number type

// export function makeState() {
//   let state: number;
//   function getState() {
//     return state;
//   }
//   function setState(x: number) {
//     state = x;
//   }
//   return { getState, setState };
// }

// TypeScript Supports if it is a string type

// export function makeState() {
//   let state: string;
//   function getState() {
//     return state;
//   }
//   function setState(x: string) {
//     state = x;
//   }
//   return { getState, setState };
// }

// TypeScript Supports if it is either string type or number type

// export function makeState() {
//   let state: string | number;
//   function getState() {
//     return state;
//   }
//   function setState(x: string | number) {
//     state = x;
//   }
//   return { getState, setState };
// }

// TypeScript Supports if it is any of type(Generics)

export function makeState<S>() {
  let state: S;
  function getState() {
    return state;
  }
  function setState(x: S) {
    state = x;
  }
  return { getState, setState };
}

// if you don't want to allow non-string or non-number values like boolean then

// export function makeState<S extends number | string>() {
//   let state: S;
//   function getState() {
//     return state;
//   }
//   function setState(x: S) {
//     state = x;
//   }
//   return { getState, setState };
// }

// if you want to make number as default

// export function makeState<S extends number | string = number>() {
//   let state: S;
//   function getState() {
//     return state;
//   }
//   function setState(x: S) {
//     state = x;
//   }
//   return { getState, setState };
// }

export function makePair() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  let pair: { first: number; second: number };
  function getPair() {
    return pair;
  }
  function setPair(x: number, y: number) {
    pair = {
      first: x,
      second: y
    };
  }
  return { getPair, setPair };
}

export function makeGenericPair<F, S>() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  let pair: { first: F; second: S };
  function getGenericPair() {
    return pair;
  }
  function setGenericPair(x: F, y: S) {
    pair = {
      first: x,
      second: y
    };
  }
  return { getGenericPair, setGenericPair };
}

export function makeExtendsGenericPair<
  F extends number | string,
  S extends boolean | F
>() {
  let pair: { first: F; second: S };
  function getExtendsGenericPair() {
    return pair;
  }
  function setExtendsGenericPair(x: F, y: S) {
    pair = {
      first: x,
      second: y
    };
  }
  return { getExtendsGenericPair, setExtendsGenericPair };
}

interface Pair<A, B> {
  first: A;
  second: B;
}

export function makeInterfacePair<F, S>() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  let pair: Pair<F, S>;
}
