import React from "react";
import {
  makeState,
  makePair,
  makeGenericPair,
  makeExtendsGenericPair
} from "./Practise";

const App: React.FC = () => {
  return <h1>Hello Typescript</h1>;
};

export default App;

const { getState, setState } = makeState();

setState("foo");
console.log(getState());

setState(1);
console.log(getState());

setState(false);
console.log(getState());

const { getPair, setPair } = makePair();

setPair(1, 2);
console.log(getPair());

setPair(3, 4);
console.log(getPair());

const { getGenericPair, setGenericPair } = makeGenericPair<number, string>();

setGenericPair(1, "foo");
console.log(getGenericPair());

const {
  getExtendsGenericPair,
  setExtendsGenericPair
} = makeExtendsGenericPair();

setExtendsGenericPair(1, true);
console.log(getExtendsGenericPair());
